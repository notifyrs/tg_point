use std::env;

use secrecy::{ExposeSecret, Secret};
use serde::Deserialize;

const PATH_TO_CONFIG: &str = "PATH_CONFIG_TG";

#[derive(Debug, Deserialize)]
pub struct SecretString(Secret<String>);

impl SecretString {
    pub fn expose_secret(&self) -> &str {
        self.0.expose_secret()
    }
}

impl Default for SecretString {
    fn default() -> Self {
        SecretString(Secret::new("".to_string()))
    }
}

#[derive(Deserialize, Debug, Default)]
pub struct Config {
    pub authorized_user_ids: Vec<u64>,
    pub telegram_bot_token: SecretString,
    pub ds_bot_address: [u8; 4],
    pub ds_bot_port: u16,
    pub path_for_log: String,
}

pub fn read_config() -> Config {
    env::var(PATH_TO_CONFIG)
        .map_err(|_| format!("{} environment variable not set", PATH_TO_CONFIG))
        .and_then(|config_path: String| std::fs::read(config_path).map_err(|err| err.to_string()))
        .and_then(|bytes| {
            toml::from_str(&String::from_utf8(bytes).unwrap_or_else(|err| {
                panic!("Failed to deserialize config: {}", err);
            }))
            .map_err(|err| err.to_string())
        })
        .unwrap_or_else(|err| {
            panic!("failed to read config: {err}");
        })
}

// TODO: tests!
