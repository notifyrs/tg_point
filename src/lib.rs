use std::{net::Ipv4Addr, sync::Arc};

use errors::{ConnectionError, ConnectionResult, ReceiveResult, SendError, SendResult};
use serde::{de::DeserializeOwned, Deserialize, Serialize};
use teloxide::{dispatching::dialogue::InMemStorage, prelude::Dialogue};
use tokio::{
    io::{AsyncReadExt, AsyncWriteExt},
    net::TcpStream,
    sync::Mutex,
};
use tracing::error;

pub mod bot;
pub mod config;
pub mod errors;

pub(crate) type NotifierDialogue = Dialogue<State, InMemStorage<State>>;

#[derive(Debug, Clone, Default)]
pub(crate) enum State {
    #[default]
    Start,
    Prepare {
        user: User,
    },
    BeginReceive {
        user: User,
        requester: ClientRequester,
    },
}
pub(crate) enum Request {
    Syn,
    Get,
    Mute,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub(crate) struct User {
    id: i64,
    tg_name: String,
    ds_name: String,
}

impl User {
    pub(crate) fn new(id: i64, tg_name: &str, ds_name: String) -> Self {
        Self {
            id,
            tg_name: tg_name.to_string(),
            ds_name,
        }
    }
}

#[derive(Serialize, Deserialize, PartialEq, Debug, Clone)]
pub struct MsgEmbed {
    title: String,
    description: String,
    footer: String,
    author: String,
}

#[derive(Debug, Serialize, Deserialize, Default)]
pub(crate) struct MsgForSend {
    channel_name: String,
    global_name: String,
    nick_name: String,
    server_name: String,
    is_bot: bool,
    mentions: Vec<String>,
    content: String,
    sticker_name: String,
    attachments: Vec<(String, String)>,
    message_link: String,
    reply_message_link: String,
    poll_name: String,
    embeds: Vec<MsgEmbed>,
}

pub(crate) async fn send_command(data: &str, writer: &mut TcpStream) -> SendResult {
    let bytes = data.as_bytes();
    let len = bytes.len() as u32;
    let len_bytes = len.to_be_bytes();
    writer.write_all(&len_bytes).await?;
    writer.write_all(bytes).await?;
    Ok(())
}

pub(crate) async fn receive<R>(reader: &mut TcpStream) -> ReceiveResult<R>
where
    R: DeserializeOwned,
{
    type Output<R> = R;
    let mut buf = [0; 4];
    reader.read_exact(&mut buf).await?;
    let len_buf: u32 = u32::from_be_bytes(buf);
    let mut buf = vec![0; len_buf as _];
    reader.read_exact(&mut buf).await?;
    let msg: Output<R> = bincode::deserialize(&buf)?;
    Ok(msg)
}

#[derive(Default, Debug, Clone)]
pub(crate) struct ClientRequester {
    tcp: Option<Arc<Mutex<Result<TcpStream, std::io::Error>>>>,
}

impl ClientRequester {
    pub(crate) async fn attach_socket(self, socket_addr: (Ipv4Addr, u16)) -> Self {
        let connection = Arc::new(Mutex::new(TcpStream::connect(socket_addr).await));

        Self {
            tcp: Some(connection),
        }
    }

    pub(crate) async fn check_alive_connection(&self) -> ConnectionResult {
        if let Some(tcp) = &self.tcp {
            let guard_socket = tcp.lock().await;
            let socket = guard_socket.as_ref();

            match socket {
                Ok(_) => Ok(()),
                Err(error) => {
                    error!("No connection::{}", error.to_string());
                    Err(ConnectionError::BadConnection)
                }
            }
        } else {
            Err(ConnectionError::BadConnection)
        }
    }

    pub(crate) async fn send_request(&self, request: Request, user_id: String) -> SendResult {
        if let Some(clonned_tcp) = &self.tcp {
            let mut guard_socket = clonned_tcp.lock().await;
            let socket = guard_socket.as_mut();

            match socket {
                Ok(tcp) => match request {
                    Request::Syn => {
                        let request = format!("syn///{}", user_id);
                        send_command(&request, tcp).await?;
                    }
                    Request::Get => {
                        send_command("get", tcp).await?;
                    }
                    Request::Mute => {
                        send_command("mute", tcp).await?;
                    }
                },
                Err(error) => {
                    error!("The command cannot be sended::{}", error.to_string());
                    return Err(SendError::BadRequest);
                }
            }
        }
        Ok(())
    }

    pub(crate) async fn receive_response<R>(&self) -> ReceiveResult<Option<R>>
    where
        R: DeserializeOwned,
    {
        let msg = if let Some(clonned_tcp) = &self.tcp {
            let mut guard_socket = clonned_tcp.lock().await;
            let socket = guard_socket.as_mut();

            match socket {
                Ok(tcp) => {
                    let receive_result = receive::<R>(tcp).await;

                    match receive_result {
                        Ok(result) => Some(result),
                        Err(e) => {
                            error!("{}", e.to_string());
                            None
                        }
                    }
                }
                Err(error) => {
                    error!("The message cannot be received::{}", error.to_string());
                    None
                }
            }
        } else {
            error!("While parsing TcpStream value");
            None
        };

        Ok(msg)
    }
}

pub(crate) fn handle_received(received_msg: &Option<MsgForSend>) -> String {
    let mut msg_for_send = String::new();

    if let Some(msg) = received_msg {
        let mentions = if !msg.mentions.is_empty() {
            msg.mentions.join(",")
        } else {
            String::default()
        };

        let bot_mark = if msg.is_bot {
            String::from("🤖")
        } else {
            String::default()
        };

        let content = &msg.content;

        let embeds = &msg
            .embeds
            .iter()
            .map(|embed| {
                format!(
                    "<pre>{}\n{}\n{}</pre>\n",
                    embed.title, embed.description, embed.footer
                )
            })
            .collect::<Vec<String>>()
            .concat();

        if !msg.channel_name.is_empty() {
            let part = format!("<b>Channel:</b> {}\n", msg.channel_name);
            msg_for_send.push_str(&part);
        }

        if !msg.global_name.is_empty() {
            if !msg.server_name.is_empty() && msg.global_name == msg.server_name {
                let part = format!("<b>Username:</b> {} {}\n", bot_mark, msg.server_name);
                msg_for_send.push_str(&part);
            } else if !msg.server_name.is_empty() && msg.global_name != msg.server_name {
                let part = format!(
                    "<b>Username:</b> {} <b>{}</b>, <i>a.k.a</i> <b>{}</b>\n",
                    bot_mark, msg.server_name, msg.global_name
                );
                msg_for_send.push_str(&part);
            } else {
                let part = format!("<b>Username:</b> {} {}\n", bot_mark, msg.global_name);
                msg_for_send.push_str(&part);
            }
        } else {
            let part = format!("<b>Username:</b> {} {}\n", bot_mark, msg.nick_name);
            msg_for_send.push_str(&part);
        }

        fn matching_embed(true_value: String, msg: &MsgForSend, embeds: &String) -> String {
            match &msg.embeds.is_empty() {
                true => true_value,
                false => true_value + "Embed message:\n" + embeds,
            }
        }

        if !content.is_empty() && msg.sticker_name.is_empty() && msg.reply_message_link.is_empty() {
            let part = match mentions.is_empty() {
                true => {
                    let true_value = format!(
                        "<b><a href=\"{}\">Message</a>:</b> <pre>{}</pre>\n",
                        msg.message_link, content
                    );
                    matching_embed(true_value, msg, embeds)
                }
                false => {
                    let true_value = format!(
                        "<b><a href=\"{}\">Message</a> for{} :</b> <pre>{}</pre>\n",
                        msg.message_link, mentions, content
                    );
                    matching_embed(true_value, msg, embeds)
                }
            };
            msg_for_send.push_str(&part);
        } else if !content.is_empty()
            && !msg.sticker_name.is_empty()
            && msg.reply_message_link.is_empty()
        {
            let part = match mentions.is_empty() {
                true => {
                    let true_value = format!(
                        "<b><a href=\"{}\">Message</a> with '{}' Sticker:</b> <pre>{}</pre>\n",
                        msg.message_link, msg.sticker_name, content
                    );
                    matching_embed(true_value, msg, embeds)
                }
                false => {
                    let true_value = format!(
                        "<b><a href=\"{}\">Message</a> with '{}' Sticker for{} :</b> <pre>{}</pre>\n",
                        msg.message_link, msg.sticker_name, mentions, content
                    );
                    matching_embed(true_value, msg, embeds)
                }
            };
            msg_for_send.push_str(&part);
        } else if !content.is_empty()
            && !msg.sticker_name.is_empty()
            && !msg.reply_message_link.is_empty()
        {
            let part = match mentions.is_empty() {
                true => {
                    let true_value = format!("<b><a href=\"{}\">Reply</a> to the <a href=\"{}\">Message</a> with '{}' Sticker:</b> <pre>{}</pre>\n", msg.message_link, msg.reply_message_link, msg.sticker_name, content);
                    matching_embed(true_value, msg, embeds)
                }
                false => {
                    let true_value = format!("<b><a href=\"{}\">Reply</a> to the <a href=\"{}\">Message</a> with '{}' Sticker for{} :</b> <pre>{}</pre>\n", msg.message_link, msg.reply_message_link, msg.sticker_name, mentions, content);
                    matching_embed(true_value, msg, embeds)
                }
            };
            msg_for_send.push_str(&part);
        } else if content.is_empty()
            && msg.sticker_name.is_empty()
            && !msg.reply_message_link.is_empty()
        {
            let part = match mentions.is_empty() {
                true => {
                    let true_value = format!(
                        "<b><a href=\"{}\">Reply</a> to the <a href=\"{}\">Message</a></b>\n",
                        msg.message_link, msg.reply_message_link
                    );
                    matching_embed(true_value, msg, embeds)
                }
                false => {
                    let true_value = format!(
                        "<b><a href=\"{}\">Reply</a> to the <a href=\"{}\">Message</a> for{}</b>\n",
                        msg.message_link, msg.reply_message_link, mentions
                    );
                    matching_embed(true_value, msg, embeds)
                }
            };
            msg_for_send.push_str(&part);
        } else if content.is_empty()
            && !msg.sticker_name.is_empty()
            && msg.reply_message_link.is_empty()
        {
            let part = match mentions.is_empty() {
                true => {
                    let true_value = format!(
                        "<b><a href=\"{}\">Message</a> with '{}' Sticker</b>\n",
                        msg.message_link, msg.sticker_name
                    );
                    matching_embed(true_value, msg, embeds)
                }
                false => {
                    let true_value = format!(
                        "<b><a href=\"{}\">Message</a> with '{}' Sticker for{}</b>\n",
                        msg.message_link, msg.sticker_name, mentions
                    );
                    matching_embed(true_value, msg, embeds)
                }
            };
            msg_for_send.push_str(&part);
        } else if content.is_empty()
            && !msg.sticker_name.is_empty()
            && !msg.reply_message_link.is_empty()
        {
            let part = match mentions.is_empty() {
                true => {
                    let true_value = format!(
                        "<b><a href=\"{}\">Reply</a> to the <a href=\"{}\">Message</a> with '{}' Sticker</b>\n",
                        msg.message_link, msg.reply_message_link, msg.sticker_name
                    );
                    matching_embed(true_value, msg, embeds)
                }
                false => {
                    let true_value = format!(
                        "<b><a href=\"{}\">Reply</a> to the <a href=\"{}\">Message</a> with '{}' Sticker for{}</b>\n",
                        msg.message_link, msg.reply_message_link, msg.sticker_name, mentions
                    );
                    matching_embed(true_value, msg, embeds)
                }
            };
            msg_for_send.push_str(&part);
        } else if !content.is_empty()
            && msg.sticker_name.is_empty()
            && !msg.reply_message_link.is_empty()
        {
            let part = match mentions.is_empty() {
                true => {
                    let true_value = format!("<b><a href=\"{}\">Reply</a> to the <a href=\"{}\">Message</a>:</b> <pre>{}</pre>\n", msg.message_link, msg.reply_message_link, content);
                    matching_embed(true_value, msg, embeds)
                }
                false => {
                    let true_value = format!("<b><a href=\"{}\">Reply</a> to the <a href=\"{}\">Message</a> for{} :</b> <pre>{}</pre>\n", msg.message_link, msg.reply_message_link, mentions, content);
                    matching_embed(true_value, msg, embeds)
                }
            };
            msg_for_send.push_str(&part);
        } else if content.is_empty()
            && msg.sticker_name.is_empty()
            && msg.reply_message_link.is_empty()
        {
            let part = match mentions.is_empty() {
                true => {
                    let true_value = format!("<a href=\"{}\">Message link</a>\n", msg.message_link);
                    matching_embed(true_value, msg, embeds)
                }
                false => {
                    let true_value = format!(
                        "<a href=\"{}\">Message</a> <b>for{}</b>\n",
                        msg.message_link, mentions
                    );
                    matching_embed(true_value, msg, embeds)
                }
            };
            msg_for_send.push_str(&part);
        }

        if !msg.attachments.is_empty() {
            for (filename, link) in &msg.attachments {
                let part = format!("<b>Attachment:</b> <a href=\"{}\">{}</a>\n", link, filename);
                msg_for_send.push_str(&part);
            }
        }
    }

    msg_for_send
}
