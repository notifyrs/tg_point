use std::sync::Arc;

use tracing::info;

use tg_point::{bot, config, errors::BotError};

#[tokio::main]
async fn main() -> Result<(), BotError> {
    let config = Arc::new(config::read_config());

    let file_appender =
        tracing_appender::rolling::daily(config.path_for_log.clone(), "tg_point.log");
    let (non_blocking, _guard) = tracing_appender::non_blocking(file_appender);
    tracing_subscriber::fmt()
        .compact()
        .pretty()
        .with_writer(non_blocking)
        .with_ansi(false)
        .init();

    info!("Starting tg_point");

    bot::NotifierBot::new(config).await?;

    Ok(())
}
