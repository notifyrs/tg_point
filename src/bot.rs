use std::{net::Ipv4Addr, sync::Arc, time::Duration};

use teloxide::{
    dispatching::{dialogue::InMemStorage, DefaultKey, Dispatcher, HandlerExt, UpdateFilterExt},
    dptree,
    payloads::{SendMessageSetters, SetMyDescriptionSetters, SetMyShortDescriptionSetters},
    requests::Requester,
    types::{KeyboardButton, KeyboardMarkup, LinkPreviewOptions, Message, Update},
    utils::command::BotCommands,
    Bot,
};
use tracing::{error, info, warn};

use crate::{
    config,
    errors::{BotError, DialogueResult},
    handle_received, ClientRequester, MsgForSend, NotifierDialogue, Request, State, User,
};

#[derive(BotCommands, Clone)]
#[command(
    rename_rule = "lowercase",
    description = "These commands are supported:"
)]
pub enum NotifierCommand {
    #[command(description = "Dislay help info. Allowed always")]
    Help,
    #[command(
        description = "Start using bot. Allowed only at the initial state. Must use in: /start <your_discord_nickname>"
    )]
    Start(String),
    #[command(description = "Connect to discord point. Allowed only after /start")]
    Connect,
    #[command(description = "Enable notifications. Allowed only after /connect")]
    Receive,
    #[command(description = "Mute notification (i.o.w disconnect). Allowed only after /connect")]
    Mute,
}

pub struct NotifierBot {
    pub dispatcher: Dispatcher<Bot, BotError, DefaultKey>,
    pub act_bot: Bot,
}

impl NotifierBot {
    pub async fn new(config: Arc<config::Config>) -> Result<Self, BotError> {
        let act_bot = Bot::new(config.telegram_bot_token.expose_secret());

        let description = "
            This bot redirects messages from discord server.\nUse button below or commands to control.
            \nTo start enter: 
            \n/start <your_discord_nickname>
            \nP.S: discord nickname, should be entered without angle brackets 😉
        ";

        let short_description = "This is private bot redirects messages from discord server";

        act_bot
            .set_my_description()
            .description(description)
            .await?;

        act_bot
            .set_my_short_description()
            .short_description(short_description)
            .await?;

        act_bot
            .set_my_commands(NotifierCommand::bot_commands())
            .await?;

        let socket_addr = (Ipv4Addr::from(config.ds_bot_address), config.ds_bot_port);

        let handler = dptree::entry().branch(
            Update::filter_message().chain(
                dptree::filter(|msg: Message, config: Arc<config::Config>| {
                    msg.from
                        .map(|user| config.authorized_user_ids.contains(&user.id.0))
                        .unwrap_or_default()
                })
                .filter_command::<NotifierCommand>()
                .enter_dialogue::<Message, InMemStorage<State>, State>()
                .branch(dptree::case![State::Start].endpoint(start))
                .branch(dptree::case![State::Prepare { user }].endpoint(prepare))
                .branch(
                    dptree::case![State::BeginReceive { user, requester }].endpoint(begin_receive),
                ),
            ),
        );

        let mut dispatcher = Dispatcher::builder(act_bot.clone(), handler)
            .dependencies(dptree::deps![
                config,
                socket_addr,
                description,
                InMemStorage::<State>::new()
            ])
            .build();

        dispatcher.dispatch().await;

        Ok(NotifierBot {
            dispatcher,
            act_bot: act_bot.clone(),
        })
    }
}

pub(crate) async fn start(
    bot: Bot,
    dialogue: NotifierDialogue,
    msg: Message,
    command: NotifierCommand,
) -> DialogueResult {
    match command {
        NotifierCommand::Start(ds_name) => {
            if !ds_name.is_empty() {
                let user_name = msg.chat.username().unwrap_or("Unknown");

                let user_id = msg.chat.id.0;

                let user = User::new(user_id, user_name, ds_name);

                info!(?user, "Start using a bot");

                bot.send_message(msg.chat.id, "Let's try to connect!")
                    .reply_markup(make_underline_keyboard(&["/connect"]))
                    .await?;
                dialogue.update(State::Prepare { user }).await?;
            } else {
                let description = "
                    This bot redirects messages from discord server.\nUse button below or commands to control.
                    \nTo start enter: 
                    \n/start <your_discord_nickname>
                    \nP.S: discord nickname, should be entered without angle brackets 😉
                ";
                bot.send_message(msg.chat.id, description).await?;

                dialogue.reset().await?;
            }

            Ok(())
        }
        NotifierCommand::Help => {
            bot.send_message(msg.chat.id, NotifierCommand::descriptions().to_string())
                .await?;
            Ok(())
        }
        _ => {
            let help_text = "
                Command don't allowed on this state.\nAllowed commands:\n\n/start <your_discord_nickname>\n/help\n\nUse /help for more information
                ".to_string();
            bot.send_message(msg.chat.id, help_text)
                .reply_markup(make_underline_keyboard(&["/start"]))
                .await?;
            Ok(())
        }
    }
}

pub(crate) async fn prepare(
    bot: Bot,
    dialogue: NotifierDialogue,
    msg: Message,
    socket_addr: (Ipv4Addr, u16),
    user: User,
    command: NotifierCommand,
) -> DialogueResult {
    match command {
        NotifierCommand::Connect => {
            info!(?user, "Try to connect");

            let requester = ClientRequester::default().attach_socket(socket_addr).await;

            if requester.check_alive_connection().await.is_ok() {
                requester
                    .send_request(Request::Syn, user.id.to_string())
                    .await?;
                let response = requester.receive_response::<String>().await?;

                if response == Some("syn-ack".to_string()) {
                    if let Some(response) = response {
                        info!("Received: '{}' response", response);
                    }
                    let kb = make_underline_keyboard(&["/receive", "/mute"]);

                    info!(?user, "Connected");

                    bot.send_message(msg.chat.id, "Connection established!")
                        .reply_markup(kb)
                        .await?;

                    dialogue
                        .update(State::BeginReceive { user, requester })
                        .await?;
                } else if response == Some("nack".to_string()) {
                    if let Some(response) = response {
                        warn!("Received: '{}' response", response);
                    }
                    error!(?user, "Not authorized");

                    bot.send_message(msg.chat.id, "You are not authorized for that operation")
                        .reply_markup(make_underline_keyboard(&["/start"]))
                        .await?;
                    dialogue.reset().await?;
                }
            } else {
                error!(?user, "Bad connection");

                bot.send_message(msg.chat.id, "Bad connection")
                    .reply_markup(make_underline_keyboard(&["/start"]))
                    .await?;
                dialogue.reset().await?;
            }

            Ok(())
        }
        NotifierCommand::Help => {
            bot.send_message(msg.chat.id, NotifierCommand::descriptions().to_string())
                .await?;
            Ok(())
        }
        _ => {
            let help_text = "
                Command don't allowed on this state.\nAllowed commands:\n\n/connect\n/help\n\nUse /help for more information
                ".to_string();

            bot.send_message(msg.chat.id, help_text)
                .reply_markup(make_underline_keyboard(&["/connect"]))
                .await?;
            Ok(())
        }
    }
}

pub(crate) async fn begin_receive(
    bot: Bot,
    dialogue: NotifierDialogue,
    msg: Message,
    command: NotifierCommand,
    (user, requester): (User, ClientRequester),
) -> DialogueResult {
    let bot = bot.clone();

    match command {
        NotifierCommand::Receive => {
            bot.send_message(msg.chat.id, "Begin recieving messages")
                .reply_markup(make_underline_keyboard(&["/mute"]))
                .await?;

            info!(?user, "Start recieving message");

            tokio::spawn(async move {
                warn!(?user, "Begin sending GET command");

                loop {
                    tokio::time::sleep(Duration::from_secs(1)).await;
                    requester
                        .send_request(Request::Get, user.id.to_string())
                        .await?;

                    let received_result = requester.receive_response::<MsgForSend>().await;

                    let prev_opt = LinkPreviewOptions {
                        is_disabled: true,
                        url: None,
                        prefer_large_media: false,
                        prefer_small_media: true,
                        show_above_text: false,
                    };

                    match received_result {
                        Ok(result) => {
                            if result.is_none() {
                                error!(?user, "Message is 'None'. Resetting state...");

                                bot.send_message(
                                    msg.chat.id,
                                    "Something goes wrong, Try to reconnect",
                                )
                                .reply_markup(make_underline_keyboard(&["/start"]))
                                .await?;

                                dialogue.reset().await?;
                                break;
                            } else if result.as_ref().is_some_and(|x| x.channel_name.is_empty()) {
                                tokio::time::sleep(Duration::from_millis(500)).await;
                                continue;
                            } else if result
                                .as_ref()
                                .is_some_and(|x| x.global_name == user.ds_name)
                            {
                                continue;
                            } else {
                                let act_msg = handle_received(&result);
                                bot.send_message(msg.chat.id, act_msg.to_string())
                                    .parse_mode(teloxide::types::ParseMode::Html)
                                    .link_preview_options(prev_opt)
                                    .await?;
                            }
                        }
                        Err(error) => {
                            error!(?user, "{}", error.to_string());

                            bot.send_message(msg.chat.id, "Something goes wrong, Try to reconnect")
                                .reply_markup(make_underline_keyboard(&["/start"]))
                                .await?;

                            dialogue.reset().await?;
                            break;
                        }
                    }
                }
                Ok::<_, BotError>(())
            });
        }
        NotifierCommand::Mute => {
            tokio::spawn(async move {
                warn!(?user, "Send MUTE command");
                tokio::spawn(async move {
                    requester
                        .send_request(Request::Mute, user.id.to_string())
                        .await?;
                    Ok::<_, BotError>(())
                });

                bot.send_message(msg.chat.id, "Gotta go way back for that")
                    .reply_markup(make_underline_keyboard(&["/start"]))
                    .await?;
                dialogue.exit().await?;
                dialogue.reset().await?;

                info!(?user, "Stop recieving message");
                Ok::<_, BotError>(())
            });
        }
        NotifierCommand::Help => {
            bot.send_message(msg.chat.id, NotifierCommand::descriptions().to_string())
                .await?;
        }
        _ => {
            let help_text = "
                Command don't allowed on this state.\nAllowed commands:\n\n/receive\n/mute\n/ignore\n/help\n\nUse /help for more information
                ".to_string();
            bot.send_message(msg.chat.id, help_text)
                .reply_markup(make_underline_keyboard(&["/receive", "/mute"]))
                .await?;
        }
    }

    Ok(())
}

pub fn make_underline_keyboard(buttons: &[&str]) -> KeyboardMarkup {
    let mut keyboard: Vec<Vec<KeyboardButton>> = vec![];

    for button in buttons.chunks(3) {
        let row = button
            .iter()
            .map(|&btn| KeyboardButton::new(btn.to_string()))
            .collect::<Vec<KeyboardButton>>();

        keyboard.push(row);
    }

    KeyboardMarkup::new(keyboard).resize_keyboard()
}
