use std::io;

use teloxide::{dispatching::dialogue::InMemStorageError, RequestError};
use thiserror::Error;

pub type SendResult = Result<(), SendError>;
pub type ReceiveResult<R> = Result<R, ReceiveError>;
pub type ConnectionResult = Result<(), ConnectionError>;
pub type DialogueResult = Result<(), BotError>;

#[derive(Debug, Error)]
pub enum SendError {
    #[error("{0}")]
    Io(#[from] io::Error),
    #[error("Bad request")]
    BadRequest,
}

#[derive(Debug, Error)]
pub enum ReceiveError {
    #[error("{0}")]
    Io(#[from] io::Error),
    #[error("{0}")]
    Bincode(#[from] bincode::Error),
    #[error("Bad encoding")]
    BadEncoding,
}

#[derive(Debug, Error)]
pub enum ConnectionError {
    #[error("Bad connection")]
    BadConnection,
    #[error(transparent)]
    RequestError(#[from] RequestError),
}

#[derive(Debug, Error)]
pub enum BotError {
    #[error(transparent)]
    SendError(#[from] SendError),
    #[error(transparent)]
    ReceiveError(#[from] ReceiveError),
    #[error(transparent)]
    DialogueError(#[from] InMemStorageError),
    #[error(transparent)]
    ConnectionError(#[from] ConnectionError),
    #[error(transparent)]
    RequestError(#[from] RequestError),
}
