# Tg-point - the bot that is responsible for receiving  notifications in the telegram

> **Note**: this bot works only in in a bundle with **ds-point**

## What you need to set up and run bot

> All  configuration  is done using  ```Config.toml```

- First of all, create bot with https://t.me/BotFather and put token in you ```Config.toml``` as shown in the example file

	> Example, you will find in repo ```Config_example.toml```

- In example you will see field ```authorized_user_ids```. This is an array of IDs telegram users, that can be used by the bot

	> Fill  array of user  IDs  that  will  use  it  in the future. 
	> If you don't know your ID or ID your users, you can use simple telegram bots, that will show you this ID
	
- Field ```ds_bot_address``` - array of four digits of the ip address separated by a comma

	> This is the address that the bot will be connecting to
    > In simple terms, the address of the ds-point

- Field ```ds_bot_port``` - port

- Field ```path_for_log``` - path, where will be stored your logs

	> Log file rotate daily

After  you  create  and  fill in your  Config.toml file,  place  it  in a convenient  location  and  export the path  to  env
```bash
export PATH_CONFIG_TG=/your/path/to/Config.toml
```

| Now you can    |									         |
|----------------|-------------------------------------------|
|Run             |```cargo run```                            |
|Build & Run     |```cargo build && ./path/to/<out> ```|
